#!/usr/bin/env bash
#
# This script shows all containers. The script determines which
# containerization software is in use from the current working directory.
#
#########################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

echo "$cmd stats"
$cmd stats
