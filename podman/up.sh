#!/usr/bin/env bash
#
# This script start containers in development environment.
#
#####################################################################
podman_up() {
    echo "Starting containers..."

    c="sisiya-backend-nginx"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      podman run -dt --name="$c" --network=host \
        --mount type=bind,src=./nginx/nginx.conf,dst=/etc/nginx/nginx.conf,ro=true \
        --mount type=bind,src=./nginx/conf.d,dst=/etc/nginx/conf.d,ro=true \
        --mount type=bind,src=local_tmp_files/log/nginx,dst=/var/log/nginx \
        docker.io/nginx:1.26.2-alpine || exit 1
    fi

    c="sisiya-backend-db"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      podman run -dt --name="$c" --network=host \
        -e POSTGRES_USER=dbuser -e POSTGRES_PASSWORD=dbuser1 -e POSTGRES_DB=db \
        --mount type=volume,src=sisiya_backend_db,dst=/var/lib/postgresql/data \
        docker.io/postgres:16.4-alpine3.20 || exit 1
    fi

    c="sisiya-backend-redis"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      # podman run -dt --name="$c" --network=host \
      podman run -dt --name="$c" --publish 26379:6379 \
        docker.io/redis:7.2.5-alpine3.20 redis-server --appendonly yes
    fi

    c="sisiya-backend"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      podman run -dt --name="$c" --network=host \
        -e DB_HOST=127.0.0.1 -e DB_NAME=db -e DB_USER=dbuser -e DB_PASSWORD=dbuser1 \
        -e REDIS_URL=redis://127.0.0.1:26379 \
        -e API_URL=http://127.0.0.1:29292 \
        --mount type=bind,src=../app,dst=/usr/src/app \
        --mount type=bind,src=/etc/localtime,dst=/etc/localtime,ro=true \
        --entrypoint='["/entrypoint.sh", "rerun", "--ignore", "**/*.{js,css,html}", "--background", "rackup --host 0.0.0.0 --port 29292"]' \
        localhost/sisiya-backend:latest
    fi

    # resque-web is not compatable with resque version 2
    # c="sisiya-backend-resque-web"
    # if ! podman container exists "$c" ; then
    #   echo "Starting container [$c] ..."
    #   podman run -dt --name="$c" --network=host \
    #     localhost/sisiya-backend-resque-web:2.6.0 || exit 1
    # fi

    c="sisiya-backend-swagger-ui"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      podman run -dt --name="$c" --network=host \
        -e API_URL=http://sisiya-backend.localdomain:8082/api/v1/definition \
        -e PORT=28080 \
        docker.io/swaggerapi/swagger-ui:v5.17.14 || exit 1
    fi
}
###############################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

MY_UID=$(id -u)
MY_GID=$(id -g)
export MY_UID MY_GID
# echo "MY_UID=$MY_UID MY_GID=$MY_GID"

make create_volume

mkdir -p local_tmp_files/log/nginx

case "$cmd" in
    docker)
        docker compose -f docker-compose.yml -f docker-compose-dev.yml up -d "$@"
        ;;
    podman)
        podman_up
        ;;
    *)
        echo "Unknown container software: $cmd"
        ;;
esac

make ps