#!/bin/bash
#
# This script creates container volume in development environment.
#
#####################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

# create a volume for the databse
v="sisiya_backend_db"

if ! "$cmd" volume inspect "$v" >/dev/null 2>&1 ; then
  echo "$cmd volume create $v"
  "$cmd" volume create "$v"
else
  echo "$0: Volume already exists: $v"
fi
