#!/usr/bin/env bash
#
# This script scans container images for valnurabilities.
#
###########################################################################
### defaults
conf_file=image.conf
number_of_images=0
project_name=project1
image_names[0]="${project_name}"
image_versions[0]="__FROM_GIT__"
SONAR_URL="https://sonarqube.example.com"
### end of defaults
###########################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

if [[ -z "$SONAR_TOKEN" ]]; then
	echo "$0: SONAR_TOKEN is not defined! Please define and export it."
	exit 1
fi

if [[ ! -f $conf_file ]]; then
	echo "$0: image.conf file does not exist!"
	exit 1
fi

# source the conf
# shellcheck disable=SC1090
. $conf_file
 
version_str=$(git describe)
declare -i i=0
while [ $i -lt $number_of_images ]
do
	s="${image_names[$i]}"
	v="${image_versions[$i]}"
	if [ "$v" = "__FROM_GIT__" ]; then
		v=$version_str
	fi
	echo "$0: Checking image $s:$v..."
	if ! "$cmd" image ls | grep "$s" | grep "$v" ; then
		echo "$0: Container image $s:$v does not exist! Please build it first."
	else
		echo "$0: Scanning image $s:$v..."
		sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.projectKey=$project_name -Dsonar.projectBaseDir=../app -Dsonar.ruby.coverage.reportPaths=./coverage/.resultset.sonarqube.json -Dsonar.sources=. -Dsonar.host.url=$SONAR_URL -Dsonar.login="$SONAR_TOKEN"
	fi
	i=$((i+1))
done
