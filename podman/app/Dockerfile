FROM docker.io/ruby:3.3.6-alpine3.21
LABEL maintainer="The SisIYA team https://gitlab.com/a6005/sisiya-backend"

WORKDIR /usr/src/app

ENV GEM_HOME="/usr/local/bundle"
ENV PATH="$GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH"

COPY app .

COPY podman/app/entrypoint.sh /

RUN apk update \
    && apk add --no-cache --virtual build-packages git autoconf automake gcc g++ libtool make postgresql-dev libgcc libstdc++ \
    && apk add --no-cache libpq \
    && apk add --no-cache ca-certificates openssl \
    && apk add --no-cache gcompat \
    && apk upgrade \
    && update-ca-certificates \
    && bundle config --global frozen 1 \
    && bundle install \
    && gem cleanup all \
    && apk del build-packages \
    && rm -rf /var/cache/apk/*

EXPOSE 9292

HEALTHCHECK CMD rake healthcheck

ENTRYPOINT ["/entrypoint.sh"]
CMD ["rackup", "--host", "0.0.0.0"]
