#!/bin/bash
#
# This script checks the health of the application.
#
#####################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

"$cmd" exec -it sisiya-backend rake healthcheck
