#!/usr/bin/env bash
#
# This script cleans up some artifacts in the development environment.
#
#####################################################################
# this pid is generated, because in development we mount the app directory
# into the sisiya-backend container
rm -f ../app/resque.pid ../app/db_migrated.txt
