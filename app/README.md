# Install bundler

- Get bundler version used for this application

```
v=$(grep -A1 "BUNDLED WITH" Gemfile.lock | grep -v BUNDLED | tr -d " ")
```

- Install bundler

```
if ! bundle _${v}_ version >/dev/null ; then gem install bundler -v "$v" ; fi
```

- Install gem files

```
bundle _"$v"_ install
```

# Update Gemfile.lock

- When you change **Gemfile**, then you need to update the **Gemfile.lock** as well

```
bundle _"$v"_ update
```

# Using the console

You can use the **console** to get various infos. Because the environment needs
some variables set, please use the console within the container.

## Start the **console**

```
docker exec -it sisiya-backend ./console
podman exec -it sisiya-backend ./console
```

The following are some examples.

## Settings

```
pry(main)> Settings
=> #<Config::Options api=#<Config::Options title="Sisiya Backend", description="Sisiya Backend", version=1.0, contact=#<Config::Options name="Erdal Mutlu, Fatih Genç", email="info@sisiya.de", url="https://sisiya.de">>, data_dir="/home/erdal/git/sisiya-backend/app/data", default_db=#<Config::Options adapter="postgresql", encoding="utf8", database="db", host="localhost", username="dbuser", password="dbuser1", pool=5, timeout=5000>, db=#<Config::Options adapter="postgresql", encoding="utf8", database="db", host="localhost", username="dbuser", password="dbuser1", pool=5, timeout=5000>, redis=#<Config::Options url="redis://127.0.0.1:6379">, testing=#<Config::Options min_coverage=90>>
```

## Database

Get list of tables

```
DB.tables
=> [:schema_migrations, :client, :message, :system, :system_service, :service, :status, :system_service_data, :system_service_data_archive, :parent, :parent_message, :master]
```

Get all records of the **clients** table

```
pry(main)> Client.count
=> 1

pry(main)> DB[:clients]
=> #<Sequel::Postgres::Dataset: "SELECT * FROM \"clients\"">

pry(main)> DB[:clients].all
=> [{:id=>1,
  :name=>"client1.example.com",
  :ip=>"127.0.0.1",
  :api_key_hash=>"8/2WvJAXCvS5Ugs8n7kSlgGX+DkDc3CFuPmQccxmCswX+bOVH0WGF6kpDk4howbUbFx+xx3IewMav7/Qz9ouJQ==",
  :created_at=>2024-07-28 21:23:58.287443 +0200,
  :updated_at=>nil}]
```

## Models

```
pry(main)> Client.columns
=> [:id, :name, :ip, :api_key_hash, :created_at, :updated_at]

pry(main)> Client.all
=> [#<Client @values={:id=>1, :name=>"client1.example.com", ip=>"127.0.0.1", :api_key_hash=>"8/2WvJAXCvS5Ugs8n7kSlgGX+DkDc3CFuPmQccxmCswX+bOVH0WGF6kpDk4howbUbFx+xx3IewMav7/Qz9ouJQ==", :created_at=>2024-07-28 21:23:58.287443 +0200, :updated_at=>nil}>]
```

## Resque

Get info about resque

```
pry(main)>Resque.info
=> {:pending=>0,
 :processed=>2,
 :queues=>1,
 :workers=>1,
 :working=>0,
 :failed=>2,
 :servers=>["<Redis::Namespace v1.11.0 with client v5.2.0 for redis://127.0.0.1:6379/resque>"],
 :environment=>"development"}
```

Get a subset of Resque.info

```
pry(main)> Resque.info[:failed]
=> 2
pry(main)> Resque.info[:processed]
=> 2
pry(main)> Resque.info[:workers]
=> 1
pry(main)> Resque.info[:servers]
=> ["<Redis::Namespace v1.11.0 with client v5.2.0 for redis://127.0.0.1:6379/resque>"]
pry(main)> Resque.info[:environment]
=> "development"

```

Get all failed messages

```
pry(main)>Resque::Failure.all(0, Resque::Failure.count)
```

Get the last failed message

```
pry(main)>Resque::Failure.all((Resque::Failure.count - 1), 1)
```
