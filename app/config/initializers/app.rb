# frozen_string_literal: true

API_KEY_LENGTH = Settings.api_key_length
API_PATH_PREFIX = Settings.api_path_prefix
API_URL = Settings.api_url
MASTER_KEY_FILE = Settings.master_key_file
