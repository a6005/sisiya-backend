# frozen_string_literal: true

# /usr/src/app is the root directory of this app within the container
RESQUE_PID_FILE = ENV['RESQUE_PID_FILE'] || '/usr/src/app/resque.pid'
Resque.redis = REDIS_URL
