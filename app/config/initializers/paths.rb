# frozen_string_literal: true

require 'pathname'
APP_ROOT = Pathname.new(File.expand_path('../..', __dir__))
LOG_PATH = APP_ROOT.join('logs')
CONFIG_PATH = APP_ROOT.join('config')
INITIALIZERS_PATH = CONFIG_PATH.join('initializers')
SRC_PATH = APP_ROOT.join('src')
DB_PATH = SRC_PATH.join('db')
DB_MIGRATIONS_PATH = DB_PATH.join('migrations')
DB_SEEDS_PATH = SRC_PATH.join('db', 'seeds')
HELPERS_PATH = SRC_PATH.join('helpers')
INTERACTIONS_PATH = SRC_PATH.join('interactions')
MODELS_PATH = SRC_PATH.join('models')
SPEC_PATH = SRC_PATH.join('spec')
WORKERS_PATH = SRC_PATH.join('workers')
API_DEFINITION_PATH = SRC_PATH.join('api_definition')
