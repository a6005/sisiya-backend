# frozen_string_literal: true

require 'fileutils'
require 'logger'
FileUtils.mkdir_p LOG_PATH
log_file = File.new("#{LOG_PATH}/app.log", 'a+')
log_file.sync = true
APP_LOGGER = Logger.new log_file

log_level = Settings.log_level
%w[debug info warn error fatal unknown].each do |level|
  log_level = level if log_level.downcase == level
end
APP_LOGGER.level = log_level.to_sym
