# frozen_string_literal: true

# /usr/src/app is the root directory of this app within the container
#
# When there is no DB connection this file is reloaded and to avoid
# annoying warning: already initialized constant DB_MIGRATED_FILE
unless Object.const_defined?('DB_MIGRATED_FILE')
  DB_MIGRATED_FILE = ENV['DB_MIGRATED_FILE'] || '/usr/src/app/db_migrated.txt'
end

begin
  # APP_LOGGER.debug 'db:init: Connecting to DB...'
  DB = Sequel.connect(Settings.db.to_hash) unless Object.const_defined?('DB')
  DB.loggers << Logger.new(File.new("#{LOG_PATH}/db.log", 'a+'))
rescue StandardError
  APP_LOGGER.fatal 'db:init: Failed to connect to DB!'
end
