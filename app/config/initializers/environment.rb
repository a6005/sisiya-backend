# frozen_string_literal: true

require 'bundler/setup'

ENV['SESSION_SECRET'] = `echo $RANDOM | sha512sum | head -c 64`
ENVIRONMENT = ENV['ENVIRONMENT'] || (ENV['RACK_ENV'] || 'development')
ENV['RACK_ENV'] = ENV['ENVIRONMENT'] = ENVIRONMENT.to_s
Bundler.require(:default, ENVIRONMENT.to_sym)
Bundler.require(:db, ENVIRONMENT.to_sym)
Config.load_and_set_settings(Config.setting_files(CONFIG_PATH, ENVIRONMENT))
