# frozen_string_literal: true

REDIS_URL = ENV['REDIS_URL'] || Settings.redis.url
