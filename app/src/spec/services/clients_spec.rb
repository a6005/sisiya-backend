# frozen_string_literal: true

require SPEC_PATH.join('spec_helper.rb')

endpoint = 'clients'
# s_name = endpoint

RSpec.describe endpoint do
  include_context 'common'

  let(:url) { "#{api_v1_url}/#{endpoint}" }

  describe 'POST methods' do
    let(:expire_period) { 120 }
    let(:response_blank_body) { conn.post url }
    let(:response_empty_email) { conn.post url, { email: '' }.to_json }
    let(:response_empty_password) { conn.post url, { password: '' }.to_json }
    let(:response_password_empty_email) { conn.post url, { password: test_pw, email: '' }.to_json }
    let(:response_email_empty_password) { conn.post url, { password: '', email: test_email }.to_json }
    let(:response_password_non_email) { conn.post url, { password: test_pw, email: none_existent_email }.to_json }
    let(:response_valid_credentials) { conn.post url, { password: test_pw, email: test_email }.to_json }
    let(:response_valid_credentials_expire) do
      conn.post url, { password: test_pw, email: test_email, expire_period: expire_period }.to_json
    end
    # let(:claims) { JWT.decode(JSON.parse(response_valid_credentials.body)['token'], nil, false) }

    it 'should respond with json mime type' do
      # json = JSON.parse(response_blank_body.headers.to_json).with_indifferent_access
      json = JSON.parse(response_blank_body.headers.to_json)
      expect(json[content_type]).to eq(mime_json)
    end
  end
end
