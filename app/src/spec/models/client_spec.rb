# frozen_string_literal: true

require 'securerandom'

RSpec.describe 'Client Model' do
  let(:host_name) { "#{FFaker::Internet.domain_word}.#{FFaker::Internet.domain_name}" }
  let(:ip) { FFaker::Internet.ip_v4_address }

  it 'should save API key hash' do
    api_key = SecureRandom.hex(API_KEY_LENGTH)
    x = Client.create(name: host_name, ip: ip, api_key: api_key)
    expect(Digest::SHA512.base64digest(api_key)).to eq(x.api_key_hash)
  end

  describe 'db fields' do
    it 'should contain created_at and updated_at' do
      expect(Client.columns).to include :created_at, :updated_at
    end
  end
end
