# frozen_string_literal: true

ENV['ENVIRONMENT'] = 'test'
require_relative '../../config/init'
require INTERACTIONS_PATH.join('init')
require MODELS_PATH.join('init.rb')
Bundler.require(:test)
require 'simplecov'
require 'simplecov_json_formatter'
require 'net/http'
require 'net/https'

RSpec.shared_context 'common' do # rubocop:disable Metrics/BlockLength
  let(:api_v1_url) { "/#{API_PATH_PREFIX}" }
  let(:api_url) { API_URL }
  let(:admin_email) { 'admin@example.com' }
  let(:admin_pw) { 'admin123654' }
  let(:admin_user_id) { 1 }
  let(:conn) { Faraday.new(api_url, headers: { 'Content-Type' => mime_json }) }
  let(:auth_conn) { Faraday.new(api_url, headers: { 'Content-Type' => mime_json }) }
  let(:auth_url) { "#{api_v1_url}/authentication" }
  let(:content_type) { 'Content-Type' }
  let(:empty_array_json_object) { [].to_json }
  let(:invalid_token) { 'thisisaninvalidetoken' }
  let(:new_valid_pw) { 'test123456' }
  let(:none_existent_email) { 'nouser.noname@example.com' }
  let(:mime_json) { 'application/json' }
  let(:message_str) { 'message' }
  let(:test_email) { 'test@example.com' }
  let(:test_pw) { 'test123654' }
  let(:test_user_id) { 2 }
  let(:test2_email) { 'test2@example.com' }
  let(:test2_pw) { 'test123654' }
  let(:test2_user_id) { 3 }

  let(:auth_response) { conn.post auth_url, { email: test_email, password: test_pw }.to_json }
  let(:token) do
    j = JSON.parse(auth_response.body).with_indifferent_access
    j[:token].to_s
  end
end

# Generate HTML and JSON reports
SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new([
                                                                  SimpleCov::Formatter::HTMLFormatter,
                                                                  SimpleCov::Formatter::JSONFormatter
                                                                ])
SimpleCov.minimum_coverage Settings.testing.min_coverage.to_i

SimpleCov.start
SimpleCov.at_exit do
  SimpleCov.result.format!
end

RSpec.configure do |config|
  config.add_setting :api_key
  config.add_setting :mock_ip
  config.add_setting :valid_account_params

  config.before :suite do
  end
end
