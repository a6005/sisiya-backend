# frozen_string_literal: true

# require_relative './spec_helper'
require SPEC_PATH.join('spec_helper.rb')
require API_DEFINITION_PATH.join('api.rb')

describe 'API definition' do
  it 'should produce correct api definition yml' do
    def_file = 'definition.yml'
    File.write(def_file, ::Apigen::Formats::OpenAPI::V3.generate(API))
    document = Openapi3Parser.load_file(def_file)
    if document.valid?
      File.delete def_file
    else
      puts 'api_definition/api_definition.rb: There a validation errors! Please check the definition.yml'
      document.errors.map { |e| puts e }
    end
    expect(document.valid?).to be true
  end
end
