# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:client) do
      primary_key :id
      String      :name
      String      :ip
      String      :api_key_hash
      DateTime    :created_at      # Creation date / time
      DateTime    :updated_at      # Update date / time
      index       :name, unique: true
      index       :api_key_hash, unique: true
    end
  end
end
