# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:message) do
      primary_key :id
      foreign_key :client_id, :client, on_delete: :restrict, null: false
      String      :file_name    # File name where the message is stored
      DateTime    :created_at   # Creation date / time
      Boolean     :processed, default: false
      index       :created_at
      index       :client_id
    end
  end
end
