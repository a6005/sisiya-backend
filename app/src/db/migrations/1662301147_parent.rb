# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:parent) do
      primary_key	:id
      String      :parent_url
      String      :api_key
      String      :name
      Integer     :update_frequency # in seconds
      DateTime    :updated_at       # Last updated date / time
      index       :name, unique: true
    end
  end
end
