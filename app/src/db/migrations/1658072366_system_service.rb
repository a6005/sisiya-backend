# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:system_service) do
      primary_key :id
      foreign_key :system_id, :system, on_delete: :restrict, null: false
      foreign_key :service_id, :service, on_delete: :restrict, null: false
      foreign_key :status_id, :status, on_delete: :restrict, null: false
      String      :description, null: false
      Integer     :expire, null: false # Expire time in minutes
      DateTime    :created_at, null: false # Creation date / time
      DateTime    :changed_at # Status change date / time
      DateTime    :updated_at # Update date / time
      index       %i[system_id service_id], unique: true
    end
  end
end
