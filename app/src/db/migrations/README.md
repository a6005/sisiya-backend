# Generate a number for migrations file name

When you need to have a new file for migrations please use data command
to obtain an seconds since epoc:

```
date '+%s'
```

After that crete a file name, which describe the migration in the form
**number_name.rb** e.g. 1653215548_messages.rb and put your migration code
in that file.
