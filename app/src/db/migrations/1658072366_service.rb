# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:service) do
      primary_key :id
      String      :name, null: false
      DateTime    :created_at, null: false # Creation date / time
      DateTime    :updated_at # Update date / time
      index       :name, unique: true
    end
  end
end
