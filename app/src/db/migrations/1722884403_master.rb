# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:master) do
      primary_key :id
      String      :master_ip, default: ''
      String      :master_key_hash
      String      :name
      DateTime    :created_at      # Creation date / time
      DateTime    :updated_at      # Update date / time
    end
  end
end
