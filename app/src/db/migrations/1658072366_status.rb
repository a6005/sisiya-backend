# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:status) do
      primary_key :id
      String      :name, null: false
      index       :name, unique: true
    end
  end
end
