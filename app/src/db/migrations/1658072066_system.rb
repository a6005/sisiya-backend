# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:system) do
      primary_key :id
      String      :name, null: false
      Boolean     :active, default: true # the system is in use
      Boolean     :effects_global, default: true # effects global status
      String      :fqdn # Fully qualified domain name
      String      :ip # IP ddress of the system
      String      :mac # MAC address of the system
      DateTime    :created_at       # Creation date / time
      DateTime    :updated_at       # Update date / time
      index       :name, unique: true
    end
  end
end
