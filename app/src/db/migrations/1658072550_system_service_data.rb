# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:system_service_data) do
      primary_key :id
      foreign_key :system_service_id, :system_service, on_delete: :restrict, null: false
      foreign_key :status_id, :status, on_delete: :restrict, null: false
      String      :description
      DateTime    :created_at # Creation date / time
      index       %i[created_at system_service_id], unique: true
    end
  end
end
