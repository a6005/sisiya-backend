# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:parent_message) do
      primary_key :id
      foreign_key :parent_id, :parent, on_delete: :restrict, null: false
      foreign_key :message_id, :message, on_delete: :restrict, null: false
      Boolean     :processed, default: false # message is send to the parent
      index %i[parent_id message_id], unique: true
    end
  end
end
