# frozen_string_literal: true

require INITIALIZERS_PATH.join('db.rb')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')
require WORKERS_PATH.join('init')

module Interactions
  # BaseMessage class for interactions, which need API key
  class BaseMessage < ActiveInteraction::Base
    include ::Helpers::Application

    hash :headers do
      string :x_api_key, default: nil
    end

    set_callback :validate, :after, lambda {
      errors.add(:client, 'API not valid!', http_code: :unauthorized) unless current_client(headers[:x_api_key])
    }

    def client
      current_client(headers[:x_api_key])
    end
  end
end
