# frozen_string_literal: true

module Interactions
  # Run interaction and return and array for roda response
  class Runner
    class << self
      include ::Helpers::Application
      attr_accessor :headers, :body_params, :query_params, :options, :interaction

      def run(interaction, request, options = {})
        @options = options
        @interaction = interaction
        set request
        run_interaction
      end

      private

      def set(request) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
        @headers = request.env.each_with_object({}) do |(k, v), acc|
          acc[Regexp.last_match(1).downcase] = v if k =~ /^http_(.*)/i
        end
        body = request.body.read
        if body.empty?
          @body_params = nil
        else
          begin
            @body_params = JSON.parse(body).symbolize_keys
          rescue StandardError => e
            APP_LOGGER.error e.class
            APP_LOGGER.error "\t #{e.backtrace.join("\n\t ")}"
          end
        end
        @query_params = request.params
        @path_params = @options[:path_params]
      end

      def run_interaction
        check_valid @interaction.run(path_params: @path_params,
                                     query_params: @query_params,
                                     body_params: @body_params,
                                     headers: @headers)
      rescue StandardError => e
        APP_LOGGER.error e.class
        APP_LOGGER.error "\t #{e.backtrace.join("\n\t ")}"
        api_response http: :bad_request, message: e.message
      end

      def check_valid(out)
        unless out.valid?
          return api_response http: out.errors.errors.first.options[:http_code] || :bad_request,
                              message: out.errors.full_messages.to_sentence
        end

        out.result
      end
    end
  end
end
