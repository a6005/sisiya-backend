# frozen_string_literal: true

module Interactions
  # Create client interraction for clients endpoint
  class CreateClient < ::Interactions::BaseClient
    hash :body_params do
      string :name, example: 'client1.example.com', description: 'Client name'
      string :ip, example: '192.168.2.1', description: 'Client IP'
    end

    def execute
      api_key = SecureRandom.hex(API_KEY_LENGTH)
      x = Client.create(name: body_params[:name], ip: body_params[:ip], api_key: api_key)
      api_response http: :created, result: x.values
    rescue Sequel::UniqueConstraintViolation => e
      api_response http: :bad_request, message: 'Client already exists!', error: e
    end
  end
end
