# frozen_string_literal: true

module Interactions
  # Search client interraction for clients endpoint
  class GetClients < Interactions::BaseClient
    hash :query_params do
      string :ip, default: nil
      string :name, default: nil
    end

    def execute # rubocop:disable Metrics/AbcSize
      x = Client.dataset
      x  = x.where(Sequel.lit('name LIKE ?', @query_params[:name])) if @query_params[:name]
      x  = x.where(Sequel.lit('ip LIKE ?', @query_params[:ip])) if @query_params[:ip]
      x  = x.limit(@query_params[:qlimit]) if @query_params[:qlimit]
      return api_response http: :no_content if x.empty?

      api_response http: :http_ok, result: x.map(&:values)
    rescue StandardError => e
      api_response http: :internal_server_error, message: 'Something went wrong!', error: e
    end
  end
end
