# frozen_string_literal: true

require 'securerandom'

module Interactions
  # Update client interraction for clients endpoint
  class ApikeyClient < ::Interactions::BaseClient
    hash :path_params do
      integer :id
    end

    def execute # rubocop:disable Metrics/MethodLength
      x = Client.find(id: path_params[:id])
      return api_response http: :not_found, message: 'Client not found!' unless x

      begin
        x.api_key = SecureRandom.hex(API_KEY_LENGTH)
        x.save
        api_response http: :http_ok, result: { api_key: x.api_key }
      rescue Sequel::UniqueConstraintViolation => e
        api_response http: :bad_request, message: 'Unique key vialation!', error: e
      rescue StandardError => e
        api_response http: :internal_server_error, message: 'Update error!', error: e
      end
    end
  end
end
