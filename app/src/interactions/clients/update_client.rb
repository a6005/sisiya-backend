# frozen_string_literal: true

require 'securerandom'

module Interactions
  # Update client interraction for clients endpoint
  class UpdateClient < ::Interactions::BaseClient
    hash :path_params do
      integer :id
    end

    hash :body_params do
      string :ip
      string :name
    end

    def execute
      x = Client.find(id: path_params[:id])
      return errors.add(:client, 'not found!', http_code: :not_found) unless x

      begin
        x.update(body_params)
        api_response http: :no_content
      rescue Sequel::UniqueConstraintViolation => e
        api_response http: :bad_request, message: 'message: Unique key vialation!', error: e
      rescue StandardError => e
        api_response http: :internal_server_error, message: 'message: Update error!', error: e
      end
    end
  end
end
