# frozen_string_literal: true

module Interactions
  # Get client interraction for clients endpoint
  class GetClient < ::Interactions::BaseClient
    hash :path_params do
      integer :id
    end

    def execute
      x = Client.find(id: path_params[:id])
      if x.nil?
        api_response http: :not_found, message: 'Client not found!'
      else
        api_response http: :http_ok, result: x.values
      end
    rescue StandardError => e
      api_response http: :internal_server_error,
                   message: 'Something went wrong with finding the client!',
                   error: e
    end
  end
end
