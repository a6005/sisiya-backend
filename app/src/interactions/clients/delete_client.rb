# frozen_string_literal: true

module Interactions
  # Delete client interraction for clients endpoint
  class DeleteClient < ::Interactions::BaseClient
    hash :path_params do
      integer :id
    end

    def execute
      x = Client.find(id: path_params[:id])
      return api_response http: :not_found, message: 'Client not found!' unless x

      begin
        x.destroy
        api_response http: :no_content
      rescue Sequel::ForeignKeyConstraintViolation => e
        api_response http: :bad_request, message: 'Foreign key vialation!', error: e
      rescue StandardError => e
        api_response http: :internal_server_error, message: 'Delete error!', error: e
      end
    end
  end
end
