# frozen_string_literal: true

module Interactions
  # Create message interraction for messages endpoint
  class CreateMessage < ::Interactions::BaseMessage
    hash :body_params do
      string :base64_data
    end

    def find_client_by_apikey(key)
      Client.all.each do |x|
        return x if x.api_key_hash == Digest::SHA512.base64digest(key)
      end
    end

    def execute
      # The client at this point is already available via the BaseMessage class
      begin
        message = Message.create(base64_data: body_params[:base64_data], client_id: client.id)
      rescue StandardError => e
        return api_response http: :bad_request, message: 'Could not create a Message!', error: e
      end

      return api_response http: :internal_server_error, message: 'Could not create a message!' unless message

      api_response http: :created
    end
  end
end
