# frozen_string_literal: true

require HELPERS_PATH.join('application.rb')

%w[
  base_client
  base_message
  clients/apikey_client
  clients/create_client
  clients/delete_client
  clients/get_client
  clients/get_clients
  clients/update_client
  messages/create_message
  interaction_runner
].each do |s|
  require INTERACTIONS_PATH.join s
rescue StandardError
  APP_LOGGER.fatal "interactions/init.rb: Could not load interaction #{s}!"
end
