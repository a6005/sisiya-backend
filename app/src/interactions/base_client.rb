# frozen_string_literal: true

require INITIALIZERS_PATH.join('db.rb')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')

module Interactions
  # BaseClient class for interactions, which need master key
  class BaseClient < ActiveInteraction::Base
    include ::Helpers::Application

    hash :headers do
      string :x_master_key, default: nil
    end

    set_callback :validate, :after, lambda {
      errors.add(:master_key, 'not valid!', http_code: :unauthorized) unless master?(headers[:x_master_key])
    }
  end
end
