# frozen_string_literal: true

endpoint_tag = 'HealthCheck'
endpoint_path = '/healthcheck'

API.tag endpoint_tag do
  description 'Health check end point'
end

API.endpoint :healthcheck do
  description 'Check the health of the application'
  method :get
  tag endpoint_tag
  path endpoint_path

  %i[ok internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      type :healthcheck_response_object
    end
  end
end
