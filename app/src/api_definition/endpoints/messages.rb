# frozen_string_literal: true

security_type = API_SECURITY.name

endpoint_tag = 'Messages'
endpoint_path = '/messages'

API.tag endpoint_tag do
  description 'Messages end point'
end

API.endpoint :create_message do
  description 'Create a message'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    type :create_message_request_object
  end

  %i[bad_request created unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :created
        type :result
      else
        type :message
      end
    end
  end
end

ApiHelper.create_request_model(API, 'messages', 'CreateMessage')
