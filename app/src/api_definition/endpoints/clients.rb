# frozen_string_literal: true

security_type = MASTER_SECURITY.name

endpoint_tag = 'Clients'
endpoint_path = '/clients'
endpoint_id_path = '/clients/{id}'

API.tag endpoint_tag do
  description 'Clients end point'
end

API.endpoint :create_client do
  description 'Create a client'
  method :post
  tag endpoint_tag
  path endpoint_path
  security security_type
  input do
    # type :client_object
    type :create_client_request_object
  end

  %i[created bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :created
        type :client_response_object
      else
        type :message
      end
    end
  end
end

ApiHelper.create_request_model(API, 'clients', 'CreateClient')

API.endpoint :update_client do
  description 'Update a client'
  method :put
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  input do
    type :client_object
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

ApiHelper.create_request_model(API, 'clients', 'UpdateClient')

API.endpoint :get_client do
  description 'Get a client'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :client_response_object
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'clients', 'GetClient')

API.endpoint :get_clients do # rubocop:disable Metrics/BlockLength
  description 'Get clients'
  method :get
  tag endpoint_tag
  security security_type
  path endpoint_path
  query do
    name(:string?).explain do
      description 'client name (optional)'
      example 'client1.example.com'
    end

    ip(:string?).explain do
      description 'IP (optional)'
      example '192.168.1.1'
    end
  end

  %i[ok bad_request unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :client_response_object_array
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'clients', 'GetClients')

API.endpoint :delete_client do
  description 'Delete a client'
  method :delete
  tag endpoint_tag
  security security_type
  path endpoint_id_path do
    id :int32
  end

  %i[no_content bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :no_content
        type :void
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'clients', 'DeleteClient')

API.endpoint :apikey_client do
  description 'Update API key for a client'
  method :put
  tag endpoint_tag
  security security_type
  path "#{endpoint_id_path}/apikey" do
    id :int32
  end

  %i[ok bad_request not_found unauthorized internal_server_error].each do |x|
    output x do
      description HTTP_DESC[x]
      status HTTP[x]
      if x == :ok
        type :client_api_key
      else
        type :message
      end
    end
  end
end

# ApiHelper.create_request_model(API, 'clients', 'DeleteClient')
