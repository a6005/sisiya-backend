# frozen_string_literal: true

require API_DEFINITION_PATH.join('api_definition_helper.rb')
require INITIALIZERS_PATH.join('http_status_codes.rb')
require 'ostruct'

API_SECURITY = OpenStruct.new(type: 'apiKey', name: 'apiKeyAuth',
                              apikey_name: 'X-API-KEY', apikey_in: 'header')
MASTER_SECURITY = OpenStruct.new(type: 'apiKey', name: 'masterKeyAuth',
                                 apikey_name: 'X-MASTER-KEY', apikey_in: 'header')
API = Apigen::Rest::Api.new
API.title Settings.api.title
API.description Settings.api.description
API.version Settings.api.version.to_s
API.contact do
  name Settings.api.contact.name
  email Settings.api.contact.email
  url Settings.api.contact.url
end

API.server "#{API_URL}/#{API_PATH_PREFIX}"

API.security_scheme API_SECURITY.type, API_SECURITY.name do
  apikey_name 'X-API-KEY'
  apikey_in 'header'
end

API.security_scheme MASTER_SECURITY.type, MASTER_SECURITY.name do
  apikey_name 'X-MASTER-KEY'
  apikey_in 'header'
end

API.model :message do
  description 'Message Response Object'
  type :object do
    message(:string).explain do
      description 'The message'
      example 'Some message'
    end
  end
end

# API.model :result do
#   description 'Response Object'
#   type :object do
#     result(:object) do
#     end
#   end
# end

API.model :result do
  description 'Response Object'
  type :object do
    message(:string).explain do
      description 'The message'
      example 'Some message'
    end
  end
end

%w[
  healthcheck clients messages
].each do |e|
  require_relative "endpoints/#{e}"
  # require_relative "models/#{e}" if File.exist?("models/#{e}.rb")
  require_relative "models/#{e}"
end
