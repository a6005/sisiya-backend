# frozen_string_literal: true

API.model :create_healthcheck_request_object do
  description 'Create Healthcheck Request Object'
  type :object do
    name(:string).explain do
      description 'Healthcheck name'
      example 'healthcheck1.example.com'
    end
  end
end

API.model :healthcheck_response_object do
  description 'Healthcheck response object'
  type :object do
    health_check(:string).explain do
      description 'Healthcheck name'
      example 'status'
    end
  end
end

API.model :healthcheck_response_object_array do
  description 'Healthcheck array response object'
  type :array do
    type :healthcheck_response_object
  end
end
