# frozen_string_literal: true

API.model :client_object do
  description 'Client Object'
  type :object do
    name(:string).explain do
      description 'Client name'
      example 'client1.example.com'
    end

    ip(:string).explain do
      description 'Client IP'
      example '192.168.1.1'
    end
  end
end

API.model :client_api_key do
  description 'Client API key'
  type :object do
    api_key(:string).explain do
      description 'Client API key'
      example 'd1cc2ae977bf3e51b14010eef704e5ac93c66e2d5463d4fc02dd47fe44b12d7f'
    end
  end
end

# API.model :create_client_request_object do
#   description 'Create Client Request Object'
#   type :object do
#     name(:string).explain do
#       description 'Client name'
#       example 'client1.example.com'
#     end
#
#     ip(:string).explain do
#       description 'Client IP'
#       example '192.168.1.1'
#     end
#   end
# end

API.model :client_response_object do # rubocop:disable Metrics/BlockLength
  description 'Client response object'
  type :object do
    id(:int32).explain do
      description 'ID'
      example '1'
    end

    name(:string).explain do
      description 'Client name'
      example 'client1.example.com'
    end

    ip(:string).explain do
      description 'Client IP'
      example '192.168.1.1'
    end

    api_key_hash(:string).explain do
      description 'API key hash'
      example 'Pze2SPsBFEaH9mzETIehN0XoSySuaepg=='
    end

    created_at(:string).explain do
      description 'Created at'
      example '2024-07-01 10:11:16 +0200'
    end

    updated_at(:string).explain do
      description 'Updated at'
      example '2024-08-21 22:11:16 +0200'
    end
  end
end

API.model :client_response_object_array do
  description 'Client array response object'
  type :array do
    type :client_response_object
  end
end
