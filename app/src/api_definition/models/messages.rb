# frozen_string_literal: true

API.model :message_object do
  description 'Message Object'
  type :object do
    name(:string).explain do
      description 'Message name'
      example 'client1.example.com'
    end
  end
end
