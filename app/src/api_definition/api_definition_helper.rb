# frozen_string_literal: true

require 'apigen/rest'
require 'apigen/formats/openapi'
require HELPERS_PATH.join('application.rb')
require INTERACTIONS_PATH.join('init.rb')

# Api definition helper
module ApiHelper
  def self.create_request_model(api, i_dir, i_name) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    require INTERACTIONS_PATH.join(i_dir, "#{i_name.underscore}.rb")
    interaction = Kernel.const_get("Interactions::#{i_name}")
    body_params = interaction.filters[:body_params].filters
    return if body_params.empty?

    api.model "#{i_name.underscore}_request_object".to_sym do
      description "#{i_name.underscore.split('_').join(' ')} request object"
      type :object do
        body_params.each do |k, v|
          send(k, ApiHelper.data_type(v)).explain do
            description v.options[:description]
            example v.options[:example]
          end
        end
      end
    end
  end

  def self.property_name; end

  def self.data_type(filter)
    case filter.class.name
    when 'ActiveInteraction::StringFilter'
      filter.options.key?(:default) ? :string? : :string
    when 'ActiveInteraction::IntegerFilter'
      filter.options.key?(:default) ? :int32? : :int32
    when 'ActiveInteraction::BooleanFilter'
      filter.options.key?(:default) ? :bool? : :bool
    else
      raise "filter class: #{filter.class.name} not found"
    end
  end
end
