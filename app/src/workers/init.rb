# frozen_string_literal: true

%w[
  message parent_updater
].each do |s|
  require WORKERS_PATH.join s
rescue StandardError
  APP_LOGGER.fatal "workers/init.rb: Can not load worker #{s}!"
end
