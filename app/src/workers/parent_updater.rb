# frozen_string_literal: true

module Workers
  # Class for parentupdater worker
  class ParentUpdater
    @queue = :parent_updates
    def self.perform(parent_id)
      processed_messages_array = ::ParentMessage.select(:message_id).where(parent_id: parent_id,
                                                                           processed: true).map(:message_id)
      messages_to_process = ::Message.select(:id).exclude(id: processed_messages_array).map(:id)
      send_message(parent_id, merge_messages(messages_to_process))
      messages_to_process.each do |msg_id|
        ParentMessage.create(parent_id: parent_id, message_id: msg_id, processed: true)
      end
    end

    def self.merge_messages(messages_to_process)
      merged_messages = { data: [] }
      ::Message.where(id: messages_to_process).each do |msg|
        merge_messages[:data] << { base64_data: msg.base64_data }
      end
      merged_messages
    end

    def self.send_message(parent_id, message)
      # Send Message to parent
    end
  end
end
