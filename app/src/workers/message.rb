# frozen_string_literal: true

module Workers
  # Class for message worker
  class Message
    @queue = :messages
    def self.perform(message_id)
      ::Message.process(message_id)
    end
  end
end
