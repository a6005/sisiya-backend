# frozen_string_literal: true

require 'digest'

module Helpers
  # Common helpers
  module Application
    # http: the HTTP return code
    # result: any results e.g. client
    # message: a message describing the action, usually this is an error message
    # error: is the error object, which will be added to the message
    def api_response(http:, result: nil, message: nil, error: nil)
      # if error
      #   APP_LOGGER.error "helpers/application.rb:a_response: #{error.class}: #{error.message}\n"
      #   APP_LOGGER.error "helpers/application.rb:a_response: #{error.backtrace.join("\n\t ")}"
      # end

      out = {}
      out = result if result
      out[:message] = message if message
      out[:message] += " #{error.message}".gsub("\n", ' ') if error

      if out.empty? && http != :ok
        [HTTP[http]]
      else
        [HTTP[http], { 'Content-Type' => 'application/json' }, out.to_json]
      end
    end

    def digest(token)
      Digest::SHA512.base64digest(token)
    end

    def authorize_token(token)
      # APP_LOGGER.debug "helpers/application.rb:authorize_token: token: #{token}"
      Client.find(api_key_hash: digest(token))
    rescue StandardError => e
      APP_LOGGER.error e.backtrace
      nil
    end

    def current_client(token)
      # APP_LOGGER.debug "helpers/application.rb:current_client: token: #{token}"
      @current_client ||= authorize_token(token)
    end

    def master?(token)
      # APP_LOGGER.debug "helpers/application.rb:master? checking token: #{token}"
      return false unless token

      # APP_LOGGER.debug "helpers/application.rb:master? finding master key for token: #{token}"
      true if Master.find(master_key_hash: digest(token))
    end
  end
end
