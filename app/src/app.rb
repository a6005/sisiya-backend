# frozen_string_literal: true

require API_DEFINITION_PATH.join('api')
require INTERACTIONS_PATH.join('init')
require MODELS_PATH.join('init.rb')
require HELPERS_PATH.join('application.rb')

# Main class of the application
class App < Roda # rubocop:disable Metrics/ClassLength
  include ::Helpers::Application

  plugin :common_logger, $stdout
  plugin :halt

  use Rack::Cors do
    allowed_methods = %i[get post put delete options head]
    allow do
      origins '*'
      resource '*', headers: :any, methods: allowed_methods
    end
  end
  plugin :all_verbs
  plugin :default_headers,
         'Content-Type' => 'application/json',
         # 'Strict-Transport-Security'=>'max-age=16070400;', # Uncomment if only allowing https:// access
         'X-Frame-Options' => 'deny',
         'X-Content-Type-Options' => 'nosniff',
         'X-XSS-Protection' => '1; mode=block'

  plugin :hash_routes

  require_relative 'routes/init'

  route do |r|
    r.get ['', true] do
      'The app is running.'
    end
    r.hash_routes
  end

  def init_app
    init_db
    return unless db_ok?

    APP_LOGGER.debug 'app.rb:init_app: before migration'
    APP_LOGGER.debug DB.tables

    migrate_db unless db_migrated?
    APP_LOGGER.debug 'app.rb:init_app: after migration'
    APP_LOGGER.debug DB.tables
    init_resque unless resque_ok?
    init_master unless master_ok?
  end

  def healthy? # rubocop:disable Metrics/MethodLength
    init_app

    is_ok = true
    @app_health_info = { 'Resque workers': 'OK', 'DB connection': 'OK', 'Master key': 'OK' }

    unless resque_ok?
      is_ok = false
      @app_health_info[:'Resque workers'] = 'FAILED'
    end

    unless db_ok?
      is_ok = false
      @app_health_info[:'DB connection'] = 'FAILED'
    end

    unless master_ok?
      is_ok = false
      @app_health_info[:'Master key'] = 'FAILED'
    end

    is_ok
  end

  def db_migrated?
    APP_LOGGER.debug "app.rb:db_migrated?: Checking db migrated file: #{DB_MIGRATED_FILE}"
    File.file?(DB_MIGRATED_FILE)
  end

  def migrate_db
    return false unless db_ok?

    APP_LOGGER.debug "app.rb:migrate_db: migrating db and creating migrated file: #{DB_MIGRATED_FILE}"
    return false if system('rake db:migrate') && !File.write(DB_MIGRATED_FILE, '')

    APP_LOGGER.debug "app.rb:migrate_db: migrated db and created db migrated file: #{DB_MIGRATED_FILE}"
    # this is needed during the first initialization when there are no table created yet
    APP_LOGGER.debug 'app.rb:master_db: Loading models..'
    load MODELS_PATH.join('init.rb')
    Status.seed
    true
  end

  def db_ok?
    return false unless Object.const_defined?('DB')

    APP_LOGGER.debug 'app.rb:db_ok?: checking the db connection...'
    DB.valid_connection?(DB)
  end

  def init_db
    return true if db_ok?

    APP_LOGGER.debug 'app.rb:init_db: Initializing DB connection...'
    begin
      APP_LOGGER.error 'app.rb:init_db: DB connection is down! Trying to connect...'
      load INITIALIZERS_PATH.join('db.rb')
      # we cannot get the return code of the above load, that is why we check here
      db_ok?
    rescue Sequel::DatabaseConnectionError, Sequel::DatabaseDisconnectError
      APP_LOGGER.fatal 'app.rb:init_db_connection: DB connection failed! Giving up...'
      false
    end
  end

  def resque_ok?
    APP_LOGGER.debug "app.rb:resque_ok?: Checking pid #{RESQUE_PID_FILE}  and Resque.info ..."
    File.file?(RESQUE_PID_FILE) && Resque.info
  rescue Redis::CannotConnectError
    APP_LOGGER.debug 'app.rb:resque_ok?: Could not connect to Redis!'
    false
  end

  def init_resque
    APP_LOGGER.debug 'app.rb:init_resque: starting resque workers...'
    start_resque_workers
  end

  def start_resque_workers
    APP_LOGGER.debug 'app.rb:start_resque_workers: connecting to redis...'
    begin
      Redis.new(url: REDIS_URL).info
    rescue Redis::CannotConnectError => e
      APP_LOGGER.error "app.rb:start_resque_workers: Could not connect to Redis server! error: #{e.message}"
      return false
    end

    APP_LOGGER.debug 'app.rb:start_resque_workers: starting resque workers...'
    system("PIDFILE=#{RESQUE_PID_FILE} BACKGROUND=yes QUEUES=\"*\" rake resque:work")
  end

  def master_ok?
    return false unless db_ok?

    APP_LOGGER.debug 'app.rb:master_ok?: Checking master ...'
    return false unless DB.table_exists?(:master)

    APP_LOGGER.debug 'app.rb:master_ok?: Master.first ...'
    Master.first
  rescue NameError
    APP_LOGGER.debug 'app.rb:master_ok?: Failed to Master.first!'
    # activate singular table names, this is needed when db connection
    # fails and is available afterwards
    Sequel::Model.plugin :singular_table_names
    load MODELS_PATH.join('master.rb')
    false
  end

  def init_master
    APP_LOGGER.debug 'app.rb:init_master: initializing master...'
    return true if master_ok?

    APP_LOGGER.debug 'app.rb:init_master: creating master...'
    Master.create(master_key: SecureRandom.hex(256))
  end

  def run_interaction(interaction, options = {})
    init_app

    return api_response http: :internal_server_error, message: 'No resque workers!' unless resque_ok?
    return api_response http: :internal_server_error, message: 'No DB connection!' unless db_ok?
    return api_response http: :internal_server_error, message: 'No Master Defined!' unless master_ok?

    Interactions::Runner.run(interaction, request, options)
  end
end
