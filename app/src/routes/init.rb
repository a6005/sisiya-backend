# frozen_string_literal: true

%w[clients messages].each { |x| require_relative x }

# Class for routes
class App
  hash_routes.on 'healthcheck' do |r|
    r.get do
      return r.halt HTTP[:ok], { 'Content-Type' => 'application/json' }, @app_health_info.to_json if healthy?

      r.halt HTTP[:internal_server_error], { 'Content-Type' => 'application/json' }, @app_health_info.to_json
    end
  end

  hash_routes.on 'api', &:hash_routes

  hash_routes('/api').on 'v1', &:hash_routes

  hash_routes('/api/v1').on 'definition' do |r|
    r.get do
      ::Apigen::Formats::OpenAPI::V3.generate(API)
    end
  end

  hash_routes('/api/v1').on 'healthcheck' do |r|
    r.get do
      return r.halt HTTP[:ok], { 'Content-Type' => 'application/json' }, @app_health_info.to_json if healthy?

      r.halt HTTP[:internal_server_error], { 'Content-Type' => 'application/json' }, @app_health_info.to_json
    end
  end
end
