# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'clients' do |r|
    r.on Integer, 'apikey' do |id|
      r.put do
        r.halt(*run_interaction(Interactions::ApikeyClient, path_params: { id: id }))
      end
    end

    r.is Integer do |id|
      r.get do
        r.halt(*run_interaction(Interactions::GetClient, path_params: { id: id }))
      end
      r.put do
        r.halt(*run_interaction(Interactions::UpdateClient, path_params: { id: id }))
      end
      r.delete do
        r.halt(*run_interaction(Interactions::DeleteClient, path_params: { id: id }))
      end
    end
    r.get do
      r.halt(*run_interaction(Interactions::GetClients))
    end
    r.post do
      r.halt(*run_interaction(Interactions::CreateClient))
    end
  end
end
