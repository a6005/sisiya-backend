# frozen_string_literal: true

# Class for routes
class App
  hash_routes("/#{API_PATH_PREFIX}").on 'messages' do |r|
    r.is do
      r.post do
        r.halt(*run_interaction(Interactions::CreateMessage))
      end
    end
  end
end
