# frozen_string_literal: true

# Class for client model
class Client < Sequel::Model
  attr_accessor :api_key

  def before_save
    update_api_key
  end

  def before_update
    update_api_key
  end

  def update_api_key
    self.api_key_hash = Digest::SHA512.base64digest(api_key) if api_key
  end
end
