# frozen_string_literal: true

# initilize common plugins
if Object.const_defined?('DB')
  Sequel::Model.db = DB
  Sequel::Model.plugin :validation_helpers
  Sequel::Model.plugin :timestamps, update_on_create: true
  Sequel::Model.plugin :singular_table_names
  # load models only there are tables
  if DB.tables.count.positive?
    APP_LOGGER.debug 'models/init.rb: There are tables'
    %w[
      client message system parent parent_message service status system_service
      system_service_data system_ervice_data_archive master
    ].each do |s|
      # load model only when table exists
      if DB.table_exists?(s.to_sym)
        APP_LOGGER.debug "models/init.rb: Loading #{s}..."
        require MODELS_PATH.join s
      end
      APP_LOGGER.debug 'models/init.rb: Loading finished.'
    rescue StandardError
      APP_LOGGER.fatal "models/init.rb: Could not load model #{s}!"
      APP_LOGGER.fatal 'models/init.rb: Probably a DB or migration issue!'
    end
  end
end
