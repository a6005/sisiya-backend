# frozen_string_literal: true

# Class for system_service model
class SystemService < Sequel::Model
  plugin :after_initialize
  attr_accessor :old_status_id # store before value of status_id

  many_to_one :service
  one_to_many :system_service_data, class: :SystemServiceData
  many_to_one :system

  def after_create
    return unless old_status_id != status_id

    self.changed_at = updated_at
  end

  def after_initialize
    self.old_status_id = status_id
  end

  def before_update
    return unless old_status_id != status_id

    self.changed_at = updated_at
  end

  def self.create_or_update(system_id, service_id, status_id, expire, description) # rubocop:disable Metrics/MethodLength
    x = SystemService.find(system_id: system_id, service_id: service_id)

    if x
      if x.status_id == status_id
        x.update(status_id: status_id, expire: expire, description: description)
      else
        x.update(status_id: status_id, expire: expire, description: description,
                 changed_at: Sequel::CURRENT_TIMESTAMP)
      end
      x
    else
      SystemService.create(system_id: system_id, service_id: service_id,
                           status_id: status_id, expire: expire,
                           description: description,
                           changed_at: Sequel::CURRENT_TIMESTAMP)
    end
  end
end
