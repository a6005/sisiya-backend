# frozen_string_literal: true

# Class for status model
class Status < Sequel::Model
  def self.seed
    status = Status.find(id: 1)
    return if status

    (1..1023).each do |i|
      Status.create(name: "status #{i}")
    end
    statuses = { info: 1, ok: 2, warning: 4, error: 8, noreport: 16, unavailable: 32,
                 mwarning: 64, merror: 128, mnoreport: 256, munavailable: 512 }
    statuses.each do |k, v|
      Status.find(id: v).update(name: k.to_s)
    end
  end
end
