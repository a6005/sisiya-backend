# frozen_string_literal: true

# Class for system_service_data model
class SystemServiceData < Sequel::Model
  many_to_one :system_service

  #  def after_create
  #    s = Service.find(name: name)
  #    x = SystemService.find(system_id: system_id, service_id: service_id)
  #    if x
  #      x.description = description
  #      x.status_id = status_id
  #      x.expire = expire
  #      x.save
  #    else
  #      SystemService.create(system_id: system_id, service_id: service_id,
  #        description: status_id: status_id, expire: expire, description)
  #    end
  #  end
end
