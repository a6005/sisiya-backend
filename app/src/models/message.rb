# frozen_string_literal: true

require 'securerandom'
require 'base64'
require 'json-schema'

# Class for message model
class Message < Sequel::Model
  attr_accessor :base64_data, :json_schema

  def validate
    validates_presence [:base64_data] if new?
    validates_includes Client.map(:id), :client_id
  end

  def check_schema
    json = JSON.parse(Base64.decode64(base64_data))
    JSON::Validator.validate(APP_ROOT.join('json_schema.json').to_s, json)
    true
  rescue StandardError => e
    APP_LOGGER.error "models/messages.rb:check_schema: Malformed bas64 encoded data=#{base64_data} error=#{e.message}"
    false
  end

  def after_create
    Resque.enqueue(Workers::Message, id)
    Parent.each do |parent|
      Resque.enqueue(Workers::ParentUpdater, parent.id) if parent.needs_update?
    end
  end

  def before_create
    APP_LOGGER.debug 'models/messages.rb:before_create: Checking json schema...'
    raise 'Message:before_create: JSON schema error!' unless check_schema

    APP_LOGGER.debug 'models/messages.rb:before_create: Continue...'
    data_dir = FileUtils.mkdir_p Pathname.new(Settings.data_dir).join(Time.now.strftime('%Y-%m-%d'))
    f = "#{data_dir.first}/#{SecureRandom.uuid}"
    File.write(f, Base64.decode64(base64_data))
    self.file_name = f
  end

  def parse_message
    JSON.parse(File.read(file_name), object_class: OpenStruct)
  end

  def self.process(id)
    message = Message.find(id: id, processed: false)
    return unless message

    message_data = message.parse_message

    message_data.systems.each do |sys|
      handle_system(sys)
    end

    message.update(processed: true)
  end

  def self.handle_system(sys) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    system = System.get_or_create(sys.name)
    sys.messages.each do |msg|
      service = Service.get_or_create(msg.service)
      overall_status_id = 0 # set initially to non existent status id
      description = ''
      msg.data.each do |data|
        status = Status.find(name: data.status)
        next unless status

        overall_status_id = status.id if status.id > overall_status_id
        description += '~' unless description.empty?
        description += "#{data.status}~#{data.description}"
      end
      next unless overall_status_id

      s = SystemService.create_or_update(system.id, service.id, overall_status_id,
                                         msg.expire.to_i, description)

      SystemServiceData.create(system_service_id: s.id, status_id: overall_status_id,
                               description: description)
    end
  end
end
