# frozen_string_literal: true

# Class for system model
class System < Sequel::Model
  def self.get_or_create(name)
    x = System.find(name: name)
    return x if x

    System.create(name: name)
  end
end
