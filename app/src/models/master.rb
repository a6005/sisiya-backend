# frozen_string_literal: true

# Class for master model
class Master < Sequel::Model
  attr_accessor :master_key

  def before_save
    self.master_key_hash = Digest::SHA512.base64digest(master_key) if master_key
  end

  def after_save
    # APP_LOGGER.info "models/master.rb:after_save: master key: #{master_key}"
    # APP_LOGGER.info "models/master.rb:after_save: master key hash: #{master_key_hash}"
    File.write(MASTER_KEY_FILE, master_key)
  end
end
