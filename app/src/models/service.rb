# frozen_string_literal: true

# Class for service model
class Service < Sequel::Model
  def self.get_or_create(name)
    x = Service.find(name: name)
    return x if x

    Service.create(name: name)
  end
end
