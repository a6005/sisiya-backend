# frozen_string_literal: true

# Class for parent model
class Parent < Sequel::Model
  def needs_update?
    updated_at + update_frequency.seconds > Time.now
  end
end
