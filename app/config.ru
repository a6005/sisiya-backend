# frozen_string_literal: true

# initialize the environment
require './config/init'
require './src/app'

run(App.freeze.app)
