# sisiya-backend

A backend for SisIYA. It is going to replace the classical C++ written TCP socket based daemon approach with an HTTP API backend.

## Installation

## Building container image

Use **docker** directory for docker and **podman** directory for podman.
The following commands are the same for both docker and podman.

Build container image

```
make build
```

## Usage

### Run the application

```
make up
```

### Running containers

```
make ps
```

### Local host entries

In order to test locally add necesery lines in /etc/hosts file by executing the following commands:

```
sudo echo "127.0.0.1 sisiya-backend.localdomain" >> /etc/hosts
sudo echo "127.0.0.1 sisiya-backend-swagger.localdomain" >> /etc/hosts
```

### Master key

In order to create an API key for a client system you will need the application's
master key. The master key is generated when the application container is started
for the first time. The master key is stored within the container in a file
called **master_key.txt**. When the container is restarted the file will be lost.
If you forget to get the master key from this file or you would like to
regenarate it for some reason, use one of the following commands:

```
docker exec -it sisiya-backend rake generate_master_key
podman exec -it sisiya-backend rake generate_master_key
```

To obtain the master key use one of the following commands:

```
docker exec -it sisiya-backend cat master_key.txt
podman exec -it sisiya-backend cat master_key.txt
```

### API for a client

Use the following command to get an API key for a client. Substitude the
**<MASTER_KEY>** with the master key obtained previousely.

```
curl --request 'POST' 'http://sisiya-backend.localdomain:8082/clients' --header 'X-MASTER-KEY: <MASTER_KEY>' --header 'accept: application/json' --header 'Content-Type: application/json' --data-ascii '{ "name": "client1.example.com", "ip": "192.168.1.1" }'
```

Or open your browser and point to http://sisiya-backend-swagger.localdomain:28080

Use "clients" endpoint to create a API key for your client node. Specify your client server name

```
{
  "name": "client1.example.com"
}
```

When you get the "api_key" use it to authorization. For this use the "Authorize" button in swagger.

Create a [JSON message](./app/src/specs/example1.json) file

```
{
    "systems": [
      {
        "name": "client1.example.com",
        "messages": [
          {
            "service": "file_system",
            "expire": "15",
            "data": [
              {
                "description": "/ is 50% full",
                "status": "ok"
              },
              {
                "description": "/var is 85% (>= 85) full!",
                "status": "warning"
              },
              {
                "description": "/tmp is 93% (>=90= full!",
                "status": "error"
              }
            ]
          },
          {
            "service": "system",
            "expire": "15",
            "data": [
              {
                "description": "The system is restarted 5 days ago. Linux client1 5.15.50-1-MANJARO",
                "status": "ok"
              }
            ]
          }
        ]
      }
    ]
  }
```

```
cat example1.json | base64 -w 0
```

To send the message use the above generated encoded message with the following command:

```
curl --request 'POST' 'http://sisiya-backend.localdomain/messages' --header 'accept: application/json' --header 'X-API-KEY: 4f9345328197dc1c0dab9b50ef3c7ca8c88381776da4ff487adc24f9640cac1c' --header 'Content-Type: application/json' --data-ascii '{ "base64_data": "ewogICAgInN5c3RlbXMiOiBbCiAgICAgIHsKICAgICAgICAibmFtZSI6ICJjbGllbnQxLmV4YW1wbGUuY29tIiwKICAgICAgICAibWVzc2FnZXMiOiBbCiAgICAgICAgICB7CiAgICAgICAgICAgICJzZXJ2aWNlIjogImZpbGVfc3lzdGVtIiwKICAgICAgICAgICAgImV4cGlyZSI6ICIxNSIsCiAgICAgICAgICAgICJkYXRhIjogWwogICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICJkZXNjcmlwdGlvbiI6ICIvIGlzIDUwJSBmdWxsIiwKICAgICAgICAgICAgICAgICJzdGF0dXMiOiAib2siCiAgICAgICAgICAgICAgfSwKICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAiZGVzY3JpcHRpb24iOiAiL3ZhciBpcyA4NSUgKD49IDg1KSBmdWxsISIsCiAgICAgICAgICAgICAgICAic3RhdHVzIjogIndhcm5pbmciCiAgICAgICAgICAgICAgfSwKICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAiZGVzY3JpcHRpb24iOiAiL3RtcCBpcyA5MyUgKD49OTA9IGZ1bGwhIiwKICAgICAgICAgICAgICAgICJzdGF0dXMiOiAiZXJyb3IiCiAgICAgICAgICAgICAgfQogICAgICAgICAgICBdCiAgICAgICAgICB9LAogICAgICAgICAgewogICAgICAgICAgICAic2VydmljZSI6ICJzeXN0ZW0iLAogICAgICAgICAgICAiZXhwaXJlIjogIjE1IiwKICAgICAgICAgICAgImRhdGEiOiBbCiAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgImRlc2NyaXB0aW9uIjogIlRoZSBzeXN0ZW0gaXMgcmVzdGFydGVkIDUgZGF5cyBhZ28uIExpbnV4IGNsaWVudDEgNS4xNS41MC0xLU1BTkpBUk8iLAogICAgICAgICAgICAgICAgInN0YXR1cyI6ICJvayIKICAgICAgICAgICAgICB9CiAgICAgICAgICAgIF0KICAgICAgICAgIH0KICAgICAgICBdCiAgICAgIH0KICAgIF0KICB9Cg==" }'
```

To decode the base64 encoded message:

```
echo "ewogICAgInN5c3RlbXMiOiBbCiAgICAgIHsKICAgICAgICAibmFtZSI6ICJjbGllbnQxLmV4YW1wbGUuY29tIiwKICAgICAgICAibWVzc2FnZXMiOiBbCiAgICAgICAgICB7CiAgICAgICAgICAgICJzZXJ2aWNlIjogImZpbGVfc3lzdGVtIiwKICAgICAgICAgICAgImV4cGlyZSI6ICIxNSIsCiAgICAgICAgICAgICJkYXRhIjogWwogICAgICAgICAgICAgIHsKICAgICAgICAgICAgICAgICJkZXNjcmlwdGlvbiI6ICIvIGlzIDUwJSBmdWxsIiwKICAgICAgICAgICAgICAgICJzdGF0dXMiOiAib2siCiAgICAgICAgICAgICAgfSwKICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAiZGVzY3JpcHRpb24iOiAiL3ZhciBpcyA4NSUgKD49IDg1KSBmdWxsISIsCiAgICAgICAgICAgICAgICAic3RhdHVzIjogIndhcm5pbmciCiAgICAgICAgICAgICAgfSwKICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAiZGVzY3JpcHRpb24iOiAiL3RtcCBpcyA5MyUgKD49OTA9IGZ1bGwhIiwKICAgICAgICAgICAgICAgICJzdGF0dXMiOiAiZXJyb3IiCiAgICAgICAgICAgICAgfQogICAgICAgICAgICBdCiAgICAgICAgICB9LAogICAgICAgICAgewogICAgICAgICAgICAic2VydmljZSI6ICJzeXN0ZW0iLAogICAgICAgICAgICAiZXhwaXJlIjogIjE1IiwKICAgICAgICAgICAgImRhdGEiOiBbCiAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgImRlc2NyaXB0aW9uIjogIlRoZSBzeXN0ZW0gaXMgcmVzdGFydGVkIDUgZGF5cyBhZ28uIExpbnV4IGNsaWVudDEgNS4xNS41MC0xLU1BTkpBUk8iLAogICAgICAgICAgICAgICAgInN0YXR1cyI6ICJvayIKICAgICAgICAgICAgICB9CiAgICAgICAgICAgIF0KICAgICAgICAgIH0KICAgICAgICBdCiAgICAgIH0KICAgIF0KICB9Cg=="|base64 --decode
```

## Initializing git pre hooks

In order to use the predefined git hooks run the following command. It needs
to be run just once.

```
./utils/add_git_hooks.sh
```

## Support

Please use gitlab's issue tracker system
https://gitlab.com/a6005/sisiya-backend/-/issues if you have any issue or requests.

## Repository

The repository is in GitLab: https://gitlab.com/a6005/sisiya-backend/

## Authors and acknowledgment

Fatih GENÇ

Erdal MUTLU

## License

This software is licensed under GPLv3.
